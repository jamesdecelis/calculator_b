package mt.edu.mcast.calculator_b;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    TextView txtvDisplay;
    int num1, num2;
    char operator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtvDisplay = findViewById(R.id.txtvDisplay);
    }

    public void numbersClicked(View v){

        Button b = (Button)v;
        txtvDisplay.setText(txtvDisplay.getText() + b.getText().toString());

    }

    public void operatorClicked(View v){

        if(txtvDisplay.getText().length() > 0) {
            num1 = Integer.parseInt(txtvDisplay.getText().toString());
        }

            switch (v.getId()) {

                case R.id.btnPlus:
                    operator = '+';
                    break;
                case R.id.btnMinus:
                    operator = '-';
                    break;
                case R.id.btnMult:
                    operator = '*';
                    break;
                case R.id.btnDiv:
                    operator = '/';
                    break;

            }
            txtvDisplay.setText("");


    }

    public void calculate(View v){

        if(txtvDisplay.getText().length() > 0) {
            num2 = Integer.parseInt(txtvDisplay.getText().toString());

            int ans = 0;

            switch (operator) {

                case '+':
                    ans = num1 + num2;
                    break;
                case '-':
                    ans = num1 - num2;
                    break;
                case '*':
                    ans = num1 * num2;
                    break;
                case '/':
                    try {
                        ans = num1 / num2;
                    }catch(ArithmeticException ae){
                        ans = 0;
                        Toast.makeText(this, "Can't divide by Zero", Toast.LENGTH_LONG).show();
                    }
                    break;
            }
            txtvDisplay.setText(String.valueOf(ans));
        }

    }
}
